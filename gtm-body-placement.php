<?php
/*
Plugin Name: Google Tag Manager custom code placement 
Plugin URI: http://www.roadsidemultimedia.com
Description: Places the Google Tag Manager plugin code right after the body tag on sites using the Pagelines DMS theme or a Hybrid Framework theme. This was tested with version 1.0 of the Google Tag Manager plugin. https://wordpress.org/plugins/duracelltomi-google-tag-manager/
Version: 1.1
Author: 
Author URI: 
License: 
Bitbucket Branch: master
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/gtm-body-placement
*/

add_action('pagelines_before_site', 'dms_gtm_add_tag');
add_action('hybrid_before_html', 'dms_gtm_add_tag');
function dms_gtm_add_tag(){
  if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) {
    gtm4wp_the_gtm_tag(); 
  }
}